package com.zedfy.zedcon;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.nio.ByteBuffer;
import java.util.UUID;

public class MainActivity extends Activity implements View.OnClickListener{


    final static UUID UUID_SERVICE_PARAMS = UUID.fromString("104a3fdf-9a37-42da-897b-10c85402f201");
    final static UUID UUID_SERVICE_DATA = UUID.fromString("104a3fdf-9a37-42da-897b-10c85402f202");
    final static UUID UUID_CHARACTERISTIC_PARAMS = UUID.fromString("419de4aa-c0e6-4c4d-a0b8-2812ebc7f401");
    final static UUID UUID_CHARACTERISTIC_DATA = UUID.fromString("35c75d3c-04bb-4aed-85b2-cfbcd8057c01");

    static final int REQUEST_ENABLE_BT = 1;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothDevice device;
    private BluetoothGatt mBluetoothGatt;
    BluetoothGattCallback mGattCallback;

    ArrayAdapter<String> mArrayAdapter;
    TextView lastMessage;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        lastMessage = findViewById(R.id.lastMessage);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Searching devices");

        // Инициализация Bluetooth адаптера
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Запрос включения адаптера
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        mBluetoothAdapter.startLeScan(new BluetoothAdapter.LeScanCallback() {

            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                mArrayAdapter.add(
                        device.getName() + "\n" +
                                device.getAddress() + "\n"
                );
            }
        });

        mArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        mArrayAdapter.notifyDataSetChanged();

        ListView devices_list = findViewById(R.id.devices_list);

        devices_list.setAdapter(mArrayAdapter);
        devices_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String address = mArrayAdapter.getItem(position).split("\n")[1];
                device = mBluetoothAdapter.getRemoteDevice(address);
                mBluetoothGatt = device.connectGatt(MainActivity.this, false, mGattCallback);
            }
        });

        mGattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    lastMessage.setText("Connected to " + gatt.getDevice().getName());
                    //mBluetoothGatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    lastMessage.setText("Disconnected from " + gatt.getDevice().getName());
                }
                super.onConnectionStateChange(gatt, status, newState);
            }
        };
    }

    @Override
    protected void onDestroy() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setGlobalbrightness:
                //  How to set Global brightness -> send 6 bytes:
                // 0x21 0x00 0x07 0x02 0x10 0x00:
                // Header: 21 00 07 02 -> type: 1(command), payload_size: 2(bytes of payload), class: 7(LED common), id: 2(LED params)
                // Payload: 10 00 -> Global Brightness.

                // header = MessageType + MessageSize + MessageClass + MessageID (4 bytes)
                // body (2 bytes)
                int size = 2;
                int header =
                        Constants.MessageType_Command +
                        size*Constants.PAYLOAD_LENGHT +
                        Constants.MessageClass_LedCommon +
                        Constants.MessageID_LED_Params;
                byte[] body = {10,0};
                byte[] message = ByteBuffer.allocate(6).putInt(header).put(body).array();

                if (mBluetoothGatt != null) {
                    BluetoothGattService gattService = mBluetoothGatt.getService(UUID_SERVICE_PARAMS);
                    BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(UUID_CHARACTERISTIC_PARAMS);
                    String result;
                    if (characteristic.setValue(message)) {
                        result = "Write OK";
                    } else result = "Write Error";
                    lastMessage.setText(result);
                } else {
                    String result = "Not connected devices";
                    lastMessage.setText(result);
                }
                break;
            case R.id.disconnect:
                if (mBluetoothGatt != null) {
                    mBluetoothGatt.close();
                    mBluetoothGatt = null;
                } else {
                    String result = "Not connected devices";
                    lastMessage.setText(result);
                }
                break;
        }
    }
}
