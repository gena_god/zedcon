package com.zedfy.zedcon;

/**
 * Created by Melesh_G on 26.01.2018.
 */

abstract class Constants {
    //Message HEADER format (4 bytes)
    //Message TYPE
    public static final int MessageType_None = 0*268435456; //undefined value: should not be used in communication, for init only
    public static final int MessageType_Command = 1*268435456; //CMD: from host to device, one way
    public static final int MessageType_Event = 2*268435456; //EVNT: from device to host, one way
    public static final int MessageType_Request = 3*268435456; //RQST: request for reading data from host to device
    public static final int MessageType_Response = 4*268435456; //RESP: response from device to host by request previous request
    public static final int MessageType_Question = 5*268435456; //QEST: ask host for data
    public static final int MessageType_Answer = 6*268435456; //ANSW: answer from host to device
    public static final int MessageType_Max = 7*268435456; //maximum value -> for limits check

    //Message SIZE
    public static final int PAYLOAD_LENGHT = 65536;

    //Message CLASS
    public static final int MessageClass_None = 0*256; //undefined value: should not be used in communication, for init only
    public static final int MessageClass_FW = 1*256; //Firmware related
    public static final int MessageClass_Bootloader = 2*256; //Bootloader + FW update
    public static final int MessageClass_Device = 3*256; //Device in general
    public static final int MessageClass_BLE = 4*256; //Bluetooth LE
    public static final int MessageClass_WiFi = 5*256; //Wi-Fi
    public static final int MessageClass_HID = 6*256; //USB HID class
    public static final int MessageClass_LedCommon = 7*256; //LED related, common for all channels
    public static final int MessageClass_LedStrip = 8*256; //LED channel (strip) related to
    public static final int MessageClass_LedEffect = 9*256; //Color/light LED effects
    public static final int MessageClass_Colors = 10*256; //Favorite colors
    public static final int MessageClass_Alarm = 11*256; //Device wake-up and alarms
    public static final int MessageClass_Max = 12*256; //maximum value -> for limits check

    //Messaged IDs for class FW:
    public static final byte MessageID_FW_None = 0; //undefined value
    public static final byte MessageID_FW_Version = 1; //Firmware “FW” version (RQST-RESP)
    public static final byte MessageID_FW_UID = 2; //FW Unique ID (RQST-RESP)
    public static final byte MessageID_FW_Name = 3; //FW name (RQST-RESP)
    public static final byte MessageID_FW_Date = 4; //FW build date (RQST-RESP)
    public static final byte MessageID_FW_Update = 5; //FW update flag (CMD)
    public static final byte MessageID_FW_Max = 6; //maximum value -> for limits check

    //Messaged IDs for class Bootloader (BL): - not supported yet
    public static final byte MessageID_BL_None = 0; //undefined value
    public static final byte MessageID_BL_Version = 1; //Bootloader version (RQST-RESP,)
    public static final byte MessageID_BL_Mode = 2; //Bootloader mode (RQST-RESP, CMD)
    public static final byte MessageID_BL_Res1 = 3; //Reserved
    public static final byte MessageID_BL_Res2 = 4; //Reserved
    public static final byte MessageID_BL_Res3 = 5; //Reserved
    public static final byte MessageID_BL_Max = 6; //maximum value -> for limits check

    //Messaged IDs for class Device: updated
    public static final byte MessageID_Device_None = 0; //undefined value
    public static final byte MessageID_Device_MCU = 1; //Device’s core microcontroller info (RQST-RESP)
    public static final byte MessageID_Device_UID = 2; //Device UID, serial number (RQST-RESP)
    public static final byte MessageID_Device_CUID = 3; //Customer UID (RQST-RESP)
    public static final byte MessageID_Device_Mode = 4; //Device current mode (RQST-RESP, CMD)
    public static final byte MessageID_Device_DateTime = 5; //Device current date & time (RQST-RESP, CMD)
    public static final byte MessageID_Device_Uptime = 6; //Device up-time (RQST-RESP)
    public static final byte MessageID_Device_Info = 7; //Device info (read-only RQST-RESP)
    public static final byte MessageID_Device_MAX = 8; //maximum value -> for limits check

    //Messaged IDs for class BLE:
    public static final byte MessageID_BLE_None = 0; //undefined value
    public static final byte MessageID_BLE_Name = 1; //Bluetooth advertising name (RQST-RESP, CMD)
    public static final byte MessageID_BLE_Params = 2; //Bluetooth connection parameters (RQST-RESP, CMD)
    public static final byte MessageID_BLE_Max = 3; //maximum value -> for limits check

    //Messaged IDs for class WIFI:
    public static final byte MessageID_WIFI_None = 0; //undefined value
    public static final byte MessageID_WIFI_Name = 1; //Name of WiFi access point in AP mode (RQST-RESP, CMD)
    public static final byte MessageID_WIFI_Mode = 2; //WiFi mode (RQST-RESP, CMD)
    public static final byte MessageID_WIFI_Addr = 3; //IP address settings (RQST-RESP, CMD)
    public static final byte MessageID_WIFI_Max = 4; //maximum value -> for limits check

    //Messaged IDs for class HID – NOT IMPLEMENTED YET:
    public static final byte MessageID_HID_None = 0; //undefined value
    public static final byte MessageID_HID_Mode = 1; //USB HID device mode (RQST-RESP, CMD)
    public static final byte MessageID_HID_Name = 2; //USB HID device name (RQST-RESP, CMD)
    public static final byte MessageID_HID_Addr = 3; //USB HID device address (RQST-RESP, CMD)
    public static final byte MessageID_HID_Max = 4; //maximum value -> for limits check

    //Messaged IDs for class LED_Common:
    public static final byte MessageID_LED_None = 0; //undefined value
    public static final byte MessageID_LED_Mode = 1; //common mode for all LED channels (RQST-RESP, CMD)
    public static final byte MessageID_LED_Params = 2; //common parameters for all LED channels (RQST-RESP, CMD)
    public static final byte MessageID_LED_Max = 3; //maximum value -> for limits check

    //Messaged IDs for class LedStrip: - changed
    public static final byte MessageID_LedStrip_None = 0; //undefined value
    public static final byte MessageID_LedStrip_Mode = 1; //mode for specific LED channel (RQST-RESP, CMD)
    public static final byte MessageID_LedStrip_Params = 2; //parameters of specific LED channel (RQST-RESP, CMD)
    public static final byte MessageID_LedStrip_Type = 3; //Type and length of the LED strip connected to the LED channel (RQST-RESP, CMD)
    public static final byte MessageID_LedStrip_Max = 4; //maximum value -> for limits check

    //Messaged IDs for class LedEffect: - new
    public static final byte MessageID_Effect_None = 0; //undefined value
    public static final byte MessageID_Effect_Params = 1; //- Not supported yet, may be used later
    public static final byte MessageID_Effect_RotationModes = 2; //active light effect bitmask for ROTATION mode
    public static final byte MessageID_Effect_Max = 3; //maximum value -> for limits check

    //Messaged IDs for class FavColors: - changed
    public static final byte MessageID_Color_None = 0; //undefined value
    public static final byte MessageID_Colors_Params = 1; //mode for specific LED channel (RQST-RESP, CMD)
    public static final byte MessageID_Colors_Max = 2; //maximum value -> for limits check

    //Messaged IDs for class Alarm: - changed
    public static final byte MessageID_Alarm_None = 0; //undefined value
    public static final byte MessageID_Alarm_Mode = 1; // To delete
    public static final byte MessageID_Alarm_Params = 2; //Alarm parameters (RQST-RESP, CMD)
    public static final byte MessageID_Alarm_Max = 3; //maximum value -> for limits check

}
